package com.gsi.bitbucket.core

import com.github.scribejava.core.builder.ServiceBuilder
import com.github.scribejava.core.oauth.OAuthService
import spock.lang.Specification


class BitbucketConnectionSpec extends Specification {


    def "Make an instance of Bitbucket Connection"(){
        when:
        BitbucketConnection bbConn = new BitbucketConnection()

        then:
        bbConn instanceof BitbucketConnection


    }

    def "Make an instance of Oauth "(){
        when:
        OAuthService service = new ServiceBuilder().
                        apiKey().
                        apiSecret().
                        build()

        then:
        service instanceof OAuthService


    }

}